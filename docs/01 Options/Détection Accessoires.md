# Détection accessoires

Ce menu permet d'incorporer dans le processus de création de réservations, les accessoires des réseaux tels que les clapets coupe-feu.

# GUIDE D'UTILISATION

#### 1. Cliquez sur l’outil dans le ruban <b>BIM Extend Resa</b>

![Sélection Configuration](../images/ChoixMurs/ToolBar.PNG)

#### 2. La boite de dialogue suivante s'affiche :

![Boîte de Dialogue Choix Murs](../images/DétectionAcc/dial1.PNG)

La boîte de dialogue présente, à travers les 3 onglets, les mêmes mode de sélection pour l'ensemble des types de réseaux (Gaines, Canalisations, Chemins de câbles). Vous pouvez donc choisir d'incorporer dans le processus de création des réservations, les accessoires et/ ou les raccords pour chacun des types. Cette sélection peut être affinée en ajoutant ou retirant individuellement chaque sous catégorie présente.

#### 3. Une fois votre sélection effectuée, cliquez sur Valider




