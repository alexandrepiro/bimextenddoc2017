# Configuration

La commande Options permet de définir les propriétés d'insertion des réservations.

## Guide d'utilisation

#### 1. Cliquez sur l’outil dans le ruban <b>BIM Extend Resa</b>

![Sélection Configuration](../images/ToolBar_Option.PNG)

#### 2. La boite de dialogue suivante s'affiche :

![Configuration Onglet Général](../images/Configuration/Options_General.PNG)

<span style="color:red"><b>1.</b></span> Dans cet onglet, vous allez pouvoir éditer les options générales définissant l'insertion des réservaions:

![Configuration Onglet Général Détail](../images/Configuration/Options_General_Encart1.PNG)

<span style="color:#4286f4"><b>A.</b></span> Cette option vous permet de choisir la forme par défaut des réservations : <i>Rectangulaire, Circulaire, Suivant la frome du réseau</i>.

<span style="color:#4286f4"><b>B.</b></span> Vous pouvez également choisir le type de réservation : <i>GOE</i> (vide) ou <i>MEP</i> (pleine).

<span style="color:#4286f4"><b>C.</b></span> Choisissez ensuite l'épaisseur de rebouchage avec choix des unités.

<span style="color:#4286f4"><b>D.</b></span>  

<span style="color:#4286f4"><b>E.</b></span>  

<span style="color:#4286f4"><b>F.</b></span> Si votre projet est collaboratif, vous pouvez choisir un sous-projet par défaut dans lequel les réservations seront placées.

Vous pouvez ensuite choisir l'information <b>"LOT"</b> qui sera saisie dans chaque réservations. Cette valeur de paramètre est à définir par type de réseau. Il est également possible de différencier les réseaux suivant certains critères :

![Configuration Onglet Général Lot Identique](../images/Configuration/outil5.PNG) ![Configuration Onglet Général Lots Différents](../images/Configuration/outil6.PNG)

<span style="color:red"><b>2.</b></span> L'onglet Numérotation va permettre de définir une numérotation pour l'ensemble de vos réservations :

![Configuration Onglet Numérotation](../images/Configuration/Options_Numérotation.PNG)

<span style="color:#4286f4"><b>A.</b></span> Dans cette zone vous allez pouvoir définir la forme de la numérotation ainsi que les critères qui vont la composer.

><b>Exemple :</b>
>
>Texte - Niveau - Numérotation => RSV-N1-001 <br>
>Niveau - Zone de définition - Support - Numérotation => N1-A1-D-001 .

<span style="color:#4286f4"><b>B.</b></span> Dans la deuxième partie de l'onglet, vous allez choisir les abréviations qui vont être utilisées lors de la numérotation.

><b>Exemple :</b>
>
><u>Support :</u> Dalle = D , Voile/Cloison = V , Poutre = P <br>
><u>Niveau :</u> Niveau -1 = S1, Niveau 0 = N0 , Niveau 1 = N1 <br>
><u>Zone de définition :</u> Zone de définition 1 = A1, Zone de définition 2 = B1

<br>

<span style="color:red"><b>3.</b></span> Dans l'Onglet <b>Lien</b> vous allez choisir si les réseaux et/ou la structure sont sur un ou plusieurs liens.

![Configuration Onglet Liens](../images/Configuration/Options_Liens.PNG)

<span style="color:#4286f4"><b>A.</b></span> Si les réseaux sont présents dans un lien, cochez l'option puis sélectionnez les liens dans lesquels ils sont présents.

<span style="color:#4286f4"><b>B.</b></span> Comme pour les réseaux, si les éléments structurels (Dalles, Murs, Poutres) sont présents dans un ou plusieurs liens, cochez l'options puis sélectionnez les liens.

#### 3. Quand toutes les options sont définies, cliquez sur <b>Sauvegarder</b>.

Vous pouvez Importer/Exporter votre fichier de configuration.

![Configuration Exporter](../images/Configuration/Fichier_Exporter.PNG)

