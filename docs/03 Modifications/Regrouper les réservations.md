# Regrouper les réservations

La commande Regrouper les réservations va vous permettre de ne faire qu'une seule grande réservation à la place de plusieurs petites.

## GUIDE D'UTILISATION

#### 1. Cliquez sur l’outil dans le ruban <b>BIM Extend Resa</b>

![Regrouper une réservation toolbar](../images/Modification/Toolbar_Modification.PNG)

#### 2. Sélectionnez les réservations à regrouper puis cliquez sur Terminer.

<span style="color:red"> <b> Attention, pour fonctionner, les réservations doivent être sur le même support. </b> </span>

![Sélection resa regroupement](../images/Regrouper/regroup1.PNG)

#### 3. Une réservation est insérée à la place de celles sélectionnées :

![Regroupement nouvelle réservation](../images/Regrouper/regroup2.PNG)
<br>
![Vue coupe Réservation avant regroupement](../images/Regrouper/regroup3.PNG)
![Vue coupe Réservation après regroupement](../images/Regrouper/regroup4.PNG)

 Si la numérotation est active dans les Options, le numéro de l'une des réservations sélectionnées sera réutilisé. Idem pour le lot même en cas de réservation multi-lot.