# Emprunter Licence

Cette commande permet d'emprunter une licence afin de pouvoir utiliser l'application BimExtend Resa sans connexion internet pendant 48h.

# GUIDE D'UTILISATION

#### 1. Cliquez sur l’outil dans le ruban <b>BIM Extend Resa</b>

![Emprunter Licence](../Licence/ToolBar.PNG)


