# Réservations Automatiques

La commande Reservation permet de créer une réservation à l'intersection d'un élément structurel (Dalles/Murs/Poutres) et d'un réseau (Gaines/Canalisations/Chemins de câbles). Suivant la configuration prédéfinie avec la commande Options, la réservation s'inserera avec une forme, un type, une épaisseur de rebouchage, un lot et/ou un numéro.

## GUIDE D'UTILISATION

#### 1. Cliquez sur l'outil <i>Réservation Automatique</i> dans le Ruban <b>BIM Extend Résa</b>

![Sélection outil Réservation Automatique](../images/ToolBar_Réservation.PNG)

#### 2. La boîte de dialogue suivante s'affiche :

![ResaAuto Choix des Réseaux](../images/ResaAuto/selobjects.PNG)

Vous pouvez choisir les réseaux que vous souhaitez analyser pour détecter des intersections avec la structure.<br>
<br>
Plusieurs choix de sélection des réseaux existent : 

- L'ensemble des réseaux présent dans le projet
- Les réseaux d'un niveau défini
- Des réseaux définis manuellement

#### 3) Après avoir défini les réseaux à analyser, le tableau des réservations à insérer s'affiche :

![ResaAuto Tableau Insertion](../images/ResaAuto/Tableau_InsertionResaAuto.PNG)

<span style="color:#4286f4"><b>A.</b></span> Vous pouvez filtrer ou grouper les données :

![ResaAuto Tableau Insertion Filtre](../images/ResaAuto/filtre.PNG) ![ResaAuto Tableau Insertion Filtre](../images/ResaAuto/groupe.PNG)

<span style="color:#4286f4"><b>B.</b></span> Pour chaque ligne, vous pouvez modifier la forme de la réservations : <i>Circulaire</i> <-> <i>Rectangulaire</i>.

<span style="color:#4286f4"><b>C.</b></span> Pour chaque ligne, vous pouvez également modifier l'épaisseur de rebouchage.

#### 4) Cochez les lignes (réservations) que vous souhaitez insérer puis cliquez sur <i>Valider</i>.

#### 5) Les réservations sont alors insérées dans le projet, avec les propriétés définies dans les Options.

![ResaAuto avant insertion](../images/ResaAuto/auto2.PNG "Avant") ![ResaAuto après insertion](../images/ResaAuto/auto3.PNG "Après") ![ResaAuto propriété Resa](../images/ResaAuto/auto4.PNG "Propriétés d'une réservation")