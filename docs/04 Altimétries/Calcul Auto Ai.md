# Calcul Automatique de l'Arrase Inférieure

Cette commande permet de calculer et de <b>mettre à jour</b> automatiquement la valeur de l'arrase inférieure pour l'ensemble des réservations présentes dans le projet, dès l'instant où l'une d'elle est modifiée.

## Guide d'utilisation

#### 1. Cliquez sur l’outil dans le ruban <b>BIM Extend Resa</b>

![ToolBar Calcul Ai](../images/Ai/ToolBar.PNG)

#### 2. L'outil s'active s'il était inactif et inversement

![ToolBar Calcul Ai](../images/Ai/CalculAutoAi_ON.PNG "Outil activé" )
![ToolBar Calcul Ai](../images/Ai/CalculAutoAi_OFF.PNG "Outil désactivé" )
